#  Reporting broken clients to Sal in Go and Swift

## Go

Original Code : https://github.com/salopensource/sal-scripts/blob/broken_client/report_broken_client.go

* Not that different from Swift.
* Built-in http client is synchronous, so no need to wait for reply
* Requires some work to get native APIs, `github.com/groob/mackit/cfpref`

## Swift

Original Code: https://github.com/salopensource/sal-scripts/tree/fix_broken_clients/report_broken_client

* Basic template of using URLSession in Swift
* Simple to use built-in APIs like UserDefaults
* URLSession is asynchronous and since this is a CLI app, we use semaphores. Better style in a more sophisticated app would be to use delegate or a closure



