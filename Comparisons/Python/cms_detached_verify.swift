//  CMSTools
//  Swift Lessons
//
//  Created by Joel Rennich on 5/31/18.
//  Copyright © 2018 Orchard & Grove Inc. All rights reserved.
//

import Foundation
import Security

class CMSTools {
    
    func CheckDetached(messagePath: String, signaturePath: String, signerIndex: Int=0, trustPolicy : SecPolicy?, evaluate: Bool=true) -> (Bool, String) {
        
        // set up some variables
        
        var decoder : CMSDecoder?
        var signature: Data?
        var message : Data?
        var trust: SecPolicy = SecPolicyCreateBasicX509()
        
        var err = CMSDecoderCreate(&decoder)
        
        do {
            message = try Data.init(contentsOf: URL.init(fileURLWithPath: messagePath))
            signature = try Data.init(contentsOf: URL.init(fileURLWithPath: signaturePath))
        } catch {
            return (false, "Unable to read source files")
        }
        
        err = CMSDecoderSetDetachedContent(decoder!, message! as CFData)
        err = CMSDecoderUpdateMessage(decoder!, (signature! as NSData).bytes, signature!.count)
        err = CMSDecoderFinalizeMessage(decoder!)
        
        if err != 0 {
            return (false, "Unable to create Decoder")
        }
        
        if trustPolicy != nil {
            trust = trustPolicy!
        }
        
        var status = CMSSignerStatus.invalidSignature
        var certVerify = OSStatus.init(bitPattern: 0)
        
        err = CMSDecoderCopySignerStatus(decoder!, signerIndex, trust, evaluate, &status, nil, &certVerify)
        
        var errorString = [Int8].init()
        cssmPerror(&errorString, certVerify)
        
        if err == 0 {
            return ((status == .valid), SignerStatus(status: status))
        } else {
            return (false, "Unable to verify signature")
        }
    }
    
    // helper function to get a plaint text status from a CMSSignerStatus
    
    fileprivate func SignerStatus(status: CMSSignerStatus) -> String {
        switch status {
        case .invalidCert:
            return "Invalid Cert"
        case .invalidIndex :
            return "Invalid Index"
        case .invalidSignature :
            return "Invalid Signature"
        case .needsDetachedContent :
            return "Error with Detached Content"
        case .unsigned :
            return "Unsigned"
        case .valid :
            return "Valid"
        }
    }
}
