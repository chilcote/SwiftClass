// Lesson 1 - Variables
// Quick and dirty setting up constants and variables
// some basic manipulation of them
// and a for loop

import Cocoa

var str = "Hello, playground"

// Easy setup

// constants

let myName = "Joel"

// variables

var myOtherName = "Rennich"

// Add them

let fullName = myName + " " + myOtherName

print(fullName)

// Integers

let num = 10
let otherNum = 20
print(num + otherNum)

// Mixing types

let num = 100
let me = "Joel"

print("User: \(me) , goes to: \(String(describing: num))")

// Arrays

var names = [ "George", "Sally", "Tom", "Mary"]

names.append("Bob")

// and loop through the list

for name in names {
    print(name)
}

print("And now in reverse...")

for i in 0...(names.count - 1) {
    
    let x = (names.count - 1) - i
    print(names[x])
}

// Dictionaries

let dict = [
    "Vancouver" : "YYZ",
    "San Franciso" : "SFO",
    "Austin" : "AUS"
]

print("")

for item in dict {
    print(item)
    print("City: \(item.key) Code: \(item.value)")
}

print(dict["Vancouver"])

// Exercises:

// 1. Create two strings, add them together with a space between
// 2. Create an Int and String, get them to print out on the same line
// 3. Create an array of items, print the array out in reverse order

